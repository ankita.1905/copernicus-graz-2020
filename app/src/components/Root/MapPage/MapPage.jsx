import * as React from "react";
import { useState } from "react";
import { Map, TileLayer } from "react-leaflet";
import styled from "styled-components";
import HeatmapLayer from "react-leaflet-heatmap-layer";
import axios from "axios";
import "./map.css";
import Pollutants from "../Pollutants";
import { dateArray } from "./dateArray";
import gradientPng from "../../../icons/gradient.png";
import Header from "../Header";

const defaultLatLon = [47.06667, 15.45];
const zoom = 10;

const TimelineWrapper = styled.div`
  position: absolute;
  width: 65%;
  height: 10%;
  display: flex;
  z-index: 2000;
  background-color: #eeeeee;
  opacity: 0.7;
  top: 82%;
  text-align: center;
  border-radius: 8px;
  margin-left: 17%;
  margin-right: auto;
  overflow-x: auto;
  cursor: pointer;
  &::-webkit-scrollbar {
    width: 0;
  }
`;

const Items = styled.button`
  text-align: center;
  margin-top: 2%;
  margin-right: 2px;
  width: 15%;
  margin: 1em;
  border: 2px solid #eeeeee;
  border-radius: 3px;
  background: #f8f5f3;
  cursor: pointer;
`;

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
`;

const GradientWrapper = styled.div`
  position: absolute;
  top: 30%;
  z-index: 2000;
  margin-right: auto;
  margin-left: 6%;
  top: 40%;
  opacity: 0.7;
`;

const Gradient = styled.img`
  width: 100%;
`;

const MapPage = () => {
  const gradient = {
    0.1: "#89BDE0",
    0.2: "#96E3E6",
    0.4: "#82CEB6",
    0.6: "#FAF3A5",
    0.8: "#F5D98B",
    1.0: "#DE9A96",
  };

  let initialVal = [0, 0, ""];
  const [addressPoints, setAddressPoints] = useState([initialVal]);

  const [latlng,setLatLng] = useState([0,0]);

  const onClick = (filename) => {
    axios
      .get("http://localhost:4001/testAPI", {
        params: {
          datafile: filename,
        },
      })
      .then((res) => {
        res.data;
        setAddressPoints(res.data);
      });
  };

  const getLatLng = (e) => {
    setLatLng(e.latlng);
    console.log(latlng);
  }

  



  const TimeLineItems = dateArray.map((dates) => (
    <Items key={dates} onClick={() => onClick(dates)}>
      {dates}
    </Items>
  ));
  return (
    <Wrapper>
      <Header />
      <Pollutants />
      <Map center={defaultLatLon} zoom={zoom} onclick={(e)=>getLatLng(e)}>
        <HeatmapLayer
          points={addressPoints}
          longitudeExtractor={(m) => m[1]}
          latitudeExtractor={(m) => m[0]}
          gradient={gradient}
          intensityExtractor={(m) => parseFloat(m[2])}
        />

        <TileLayer
          url="https://api.mapbox.com/styles/v1/ankita199205/ckg2e4c1m0mze1an1zpycwll8/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYW5raXRhMTk5MjA1IiwiYSI6ImNrZzI4aDQ1NTB1ZXMycm8ybDIzajI4djIifQ.f9fJhDSzXOW2H0lp6-2OiA"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox'
        />
        <HeatmapLayer
          points={addressPoints}
          longitudeExtractor={(m) => m[1]}
          latitudeExtractor={(m) => m[0]}
          gradient={gradient}
          intensityExtractor={(m) => parseFloat(m[2])}
        />
      </Map>
      <GradientWrapper>
        <Gradient src={gradientPng} />
      </GradientWrapper>
      <TimelineWrapper>{TimeLineItems}</TimelineWrapper>
    </Wrapper>
  );
};

export default MapPage;
