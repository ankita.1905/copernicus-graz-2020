import * as React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  position: absolute;
  top: 120px;
  z-index: 2000;
  display: block;
  background-color: #eeeeee;
  opacity: 0.5;
  height: 6em;
  width: 6em;
  text-align: center;
  margin-left: 7%;
  margin-right: auto;
  border-radius: 50%;
  border-style: dashed;
  border-color: #515151;
  cursor: pointer;
`;

const Pollutant = styled.a`
  font-size: 1rem;
  font-family: inherit;
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-top: 30px;
  cursor: pointer;
`;

const Pollutants = () => {
  return (
    <Wrapper>
      <Pollutant>Pollutant Measured: NO</Pollutant>
    </Wrapper>
  );
};

export default Pollutants;
