import * as React from "react";
import styled from "styled-components";

const Wrapper = styled.nav`
  align-items: center;
  text-align: center;
  position: absolute;
  font-family: inherit;
  background-color: #eeeeee;
  margin-left: 10%;
  opacity: 0.7;
  margin-right: auto;
  height: 10%;
  width: 80%;
  top: 3%;
  border-radius: 5px;
  z-index: 2000;
`;

const HeaderTitle = styled.p`
  font-size: 1.2rem;
  font-family: "Roboto Condensed", sans-serif;
`;

const Header = () => {
  return (
    <Wrapper>
      <HeaderTitle>AirForte : Air Quality Monitoring Service </HeaderTitle>
    </Wrapper>
  );
};

export default Header;
