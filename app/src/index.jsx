import * as React from "react";
import { render } from "react-dom";
import { createGlobalStyle } from "styled-components";
import Root from "./components/Root";

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap');

body{
    font-family: 'Roboto Condensed', sans-serif;
    background: #eeeeee;
}
`;

render(
  <>
    <GlobalStyle />
    <Root />
  </>,
  document.getElementById("app")
);
